package com.tareas.asignacion_tareas.web.rest;

import com.tareas.asignacion_tareas.service.RolsService;
import com.tareas.asignacion_tareas.service.dto.RolsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RolsResource {

    @Autowired
    RolsService rolsService;

    @PostMapping("/rols")
    public ResponseEntity create(@RequestBody RolsDTO rolsDTO){
        return rolsService.create(rolsDTO);
    }

    @GetMapping("/rols")
    public List<RolsDTO> read(){
        return rolsService.read();
    }

    @PutMapping("/rols")
    public RolsDTO update(@RequestBody RolsDTO rolsDTO){
        return rolsService.update(rolsDTO);
    }

    @GetMapping("/rols/{id}")
    public Optional<RolsDTO> getById(@PathVariable Long id){
        return rolsService.getById(id);
    }
}

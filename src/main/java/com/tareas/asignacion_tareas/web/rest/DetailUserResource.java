package com.tareas.asignacion_tareas.web.rest;

import com.tareas.asignacion_tareas.service.DetailUserService;
import com.tareas.asignacion_tareas.service.dto.DetailUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DetailUserResource {

    @Autowired
    DetailUserService detailUserService;

    @GetMapping("/users/find-by-document")
    public List<DetailUserDTO> getByDocumentNumberContaining(@RequestParam(value = "document.contains",required = false) String documentNumber){
        return detailUserService.getByDocumentNumberContaining(documentNumber);
    }

    @GetMapping("/users/nameUsers")
    public List<DetailUserDTO>getByFirstNameAndLastName(@RequestParam(value = "name.contains",required = false)String firstName, String lastName){
        return detailUserService.getByFirstNameAndLastName(firstName,lastName);
    }

    @GetMapping("/users/name")
    public List<DetailUserDTO>getAllByFirstName(@RequestParam(value = "name.contains",required = false)String name){
        return detailUserService.getAllByFirstName(name);
    }
}

package com.tareas.asignacion_tareas.web.rest;

import com.tareas.asignacion_tareas.service.TareaService;
import com.tareas.asignacion_tareas.service.UsersService;
import com.tareas.asignacion_tareas.service.dto.TareaDTO;
import com.tareas.asignacion_tareas.service.dto.TareaListarDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TareaResource {

    @Autowired
    TareaService tareaService;

    @Autowired
    UsersService usersService;


    @PostMapping("/tarea")
    public ResponseEntity create (@RequestBody TareaDTO tareaDTO){
        return tareaService.create(tareaDTO);
    }

    @GetMapping("/tarea")
    public Page<TareaDTO>read(@RequestParam(value = "pageSize")Integer pageSize,
                              @RequestParam(value = "pageNumber")Integer pageNumber){
        return tareaService.read(pageSize,pageNumber);
    }

    @PutMapping("/tarea")
    public TareaDTO update(@RequestBody TareaDTO tareaDTO){
        return tareaService.update(tareaDTO);
    }

    @GetMapping("/tarea/{id}")
    public Optional<TareaDTO> getById(@PathVariable Long id){
        return tareaService.getById(id);
    }

    @GetMapping("/tarea/count")
    public Long cantidadTareas(){
        return tareaService.cantidadTareas();
    }

    @GetMapping("/tarea/tareaActiva")
    public List<TareaListarDTO> getByTareaActiva(@RequestParam(value = "tareaActiva.contains",required = false)boolean tareaActiva){
        return tareaService.getByTareaActiva(tareaActiva);
    }

    @GetMapping("/tarea/search")
    public Page<TareaListarDTO> search(@RequestParam(value = "documentNumberEmpleado",required = false)String documentNumberEmpleado,
                                       @RequestParam(name = "pageSize")Integer pageSize,
                                       @RequestParam(name = "pageNumber")Integer pageNumber){
        return tareaService.search(documentNumberEmpleado, pageSize, pageNumber);
    }

    @GetMapping("/tarea/find-by-name-empleado")
    public ResponseEntity<?> getBynameEmpleado(@RequestParam(value = "name.contains",required = false)String nameEmpleado,
                                               @RequestParam(name = "pageSize")Integer pageSize,
                                               @RequestParam(name = "pagenumber")Integer pageNumber){
        if (usersService.existsByFirstName(nameEmpleado.toUpperCase())){
            return new ResponseEntity(tareaService.getByNameEmpleado(nameEmpleado,pageSize,pageNumber), HttpStatus.OK);
        }else{
            return null;
        }
    }
}

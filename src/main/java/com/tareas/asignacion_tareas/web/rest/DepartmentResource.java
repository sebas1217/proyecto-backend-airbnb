package com.tareas.asignacion_tareas.web.rest;

import com.tareas.asignacion_tareas.service.DepartmentService;
import com.tareas.asignacion_tareas.service.dto.DepartmentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DepartmentResource {

    @Autowired
    DepartmentService departmentService;

    @PostMapping("/department")
    public ResponseEntity create(@RequestBody DepartmentDTO departmentDTO){
        return departmentService.create(departmentDTO);
    }

    @GetMapping("/department")
    public List<DepartmentDTO> read(){
        return departmentService.read();
    }

    @PutMapping("/department")
    public DepartmentDTO update(@RequestBody DepartmentDTO departmentDTO){
        return departmentService.update(departmentDTO);
    }

    @GetMapping("/department/find-By-department")
    public Optional<DepartmentDTO> getByDepartment(@RequestParam(value = "department.contains",required = false)String department){
        return departmentService.getByDepartment(department);
    }

    @GetMapping("/department/find-By-department-containing")
    public List<DepartmentDTO>getByDepartmentContaining(@RequestParam(value = "department.contains",required = false)String department){
        return departmentService.getByDepartmentContaining(department);
    }

    @GetMapping("/department/{id}")
    public Optional<DepartmentDTO>getById(@PathVariable Long id){
        return departmentService.getById(id);
    }
}

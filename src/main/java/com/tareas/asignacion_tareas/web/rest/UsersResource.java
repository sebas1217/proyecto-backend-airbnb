package com.tareas.asignacion_tareas.web.rest;


import com.tareas.asignacion_tareas.domain.Users;
import com.tareas.asignacion_tareas.service.DetailUserService;
import com.tareas.asignacion_tareas.service.UsersService;
import com.tareas.asignacion_tareas.service.dto.UsersDTO;
import com.tareas.asignacion_tareas.service.dto.UsersDTONombreCompleto;
import com.tareas.asignacion_tareas.service.transformer.UsersTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class UsersResource {

    @Autowired
    UsersService usersService;

    @Autowired
    DetailUserService detailUserService;

    @PostMapping("/users")
    public ResponseEntity<?> create(@Valid @RequestBody Users users, BindingResult bindingResult){
        UsersDTO usersDTO = null;
        Map<String,Object>response = new HashMap<>();

        if (bindingResult.hasErrors()){
            List<String> errors = bindingResult.getFieldErrors()
                    .stream()
                    .map(error->"el campo" + error.getField()+" " + error.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            UsersDTO usersDTO1 = UsersTransformer.getUsersDTOFromUsers(users);
            usersDTO = usersService.create(usersDTO);
        }catch (DataAccessException e){
            response.put("mensaje", "Error al crear un usuario");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("Mensaje"," El usuario se creo satisfactoriamente");
        response.put("user",usersDTO);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/users")
    public Iterable<UsersDTONombreCompleto>read(){
        return usersService.read();
    }

    @PutMapping("/users")
    public Users update(@RequestBody Users users){
        return usersService.update(users);
    }

    @GetMapping("/users/documentNumber/{documentNumber}")
    public ResponseEntity<?>getByDocumentNumber(@PathVariable String documentNumber){
        if (usersService.existsByDocumentNumber(documentNumber)){
            return  new ResponseEntity<>(usersService.getByDocumentNumber(documentNumber),HttpStatus.OK);
        }else{
            return null;
        }
    }


    @GetMapping("/users/findById")
    public ResponseEntity<UsersDTONombreCompleto> getById(@RequestParam(value = "id") Long id) throws IOException {
        return new ResponseEntity<>(usersService.getById(id).get(),HttpStatus.OK);
    }


    @GetMapping("/users/getByIdDetailUser")
    public ResponseEntity<?> getByIdDetailUser(@RequestParam(value = "id") Long id){
        if (detailUserService.existsByIdDetailUser(id)){
            return new ResponseEntity<>(usersService.getByIdDetailUser(id), HttpStatus.OK);
        }else{
            return null;
        }
    }

}

package com.tareas.asignacion_tareas.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tarea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private long idTarea;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime fechaTarea;

    @NotNull
    @Column(nullable = false)
    @Size(min = 2, max = 500, message = "La tarea debe contener entre 2 y 500 caracteres")
    private String descripcionTarea;

    @ManyToOne
    private Users empleado;

    @ManyToOne
    private Users jefeDirecto;

    @ManyToOne
    private Department departamento;

    @Column
    private Boolean tareaActiva;




}

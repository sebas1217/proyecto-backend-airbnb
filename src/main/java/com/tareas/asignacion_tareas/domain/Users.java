package com.tareas.asignacion_tareas.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private long idUser;

    @JoinColumn(name = "id_detail_user", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private DetailUser detailUser;

    @ManyToMany
    @JoinTable(name = "user_has_rol",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private Set<Rols> rols;
}

package com.tareas.asignacion_tareas.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
public class DetailUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long idDetailUser;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 30, message = "El tamaño del campo debe de estar entre 2 a 30 caracteres")
    @Column(nullable = false)
    private String lastName;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 30, message = "El tamaño del campo debe de estar entre 2 a 30 caracteres")
    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    @NotEmpty(message = "El campo es requerido")
    private String typeDocument;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 20, message = "El tamaño del campo debe de estar entre 6 a 20 caracteres")
    @Column(unique = true)
    private String documentNumber;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 50, message = "El tamaño del campo debe de estar entre 6 a 50 caracteres")
    @Column(nullable = false)
    @Email(message = "El email no tiene la estructura correcta")
    private String email;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 20, message = "El tamaño del campo debe de estar entre 6 a 20 caracteres")
    @Column(nullable = false)
    private String phoneNumber;

    @Size(min = 6, max = 50, message = "El tamaño del campo debe de estar entre 6 a 50 caracteres")
    private String address;
}

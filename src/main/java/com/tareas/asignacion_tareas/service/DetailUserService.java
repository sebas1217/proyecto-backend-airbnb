package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.service.dto.DetailUserDTO;

import java.util.List;

public interface DetailUserService {
    List<DetailUserDTO> getByDocumentNumberContaining(String documentNumber);

    List<DetailUserDTO> getByFirstNameAndLastName(String firstName, String lastName);

    List<DetailUserDTO> getAllByFirstName(String name);

    boolean existsByIdDetailUser(Long id);
}

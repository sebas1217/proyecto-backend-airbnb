package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.domain.Users;
import com.tareas.asignacion_tareas.service.dto.UsersDTO;
import com.tareas.asignacion_tareas.service.dto.UsersDTONombreCompleto;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UsersService {
    //get - obtener - read
    public Iterable<UsersDTONombreCompleto> read();

    //put - actualizar - update
    public Users update(Users users);

    public UsersDTO create(UsersDTO usersDTO);

    public Optional<UsersDTONombreCompleto> getById(Long id) throws IOException;

    public Optional<UsersDTO> getByDocumentNumber(String documentNumber);

    public Iterable<Users> getByDocumentNumberContaining(String documentNumber);

    public Iterable<Users> getByFirstNameAndLastName(String firstName, String lastName);

    public boolean existsByDocumentNumber (String documentNumber);

    boolean existsByFirstName (String firsName);

    public List<UsersDTO> getAllByFirstName(String name);

    Optional<UsersDTO> getByIdDetailUser (Long id);



    boolean existsByIdUser (Long id);
}

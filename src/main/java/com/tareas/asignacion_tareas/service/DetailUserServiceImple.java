package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.repository.DetailUserRepository;
import com.tareas.asignacion_tareas.service.dto.DetailUserDTO;
import com.tareas.asignacion_tareas.service.transformer.DetailUserTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DetailUserServiceImple implements DetailUserService{

    @Autowired
    DetailUserRepository detailUserRepository;


    @Override
    public List<DetailUserDTO> getByDocumentNumberContaining(String documentNumber) {
        return detailUserRepository.findByDocumentNumberContaining(documentNumber)
                .stream()
                .map(DetailUserTransformer::getDetailUserDTOFromDetailUser)
                .collect(Collectors.toList());
    }

    @Override
    public List<DetailUserDTO> getByFirstNameAndLastName(String firstName, String lastName) {
        return detailUserRepository.findAllByFirstNameAndLastNameContaining(firstName.toUpperCase(),lastName.toUpperCase())
                .stream()
                .map(DetailUserTransformer::getDetailUserDTOFromDetailUser)
                .collect(Collectors.toList());
    }

    @Override
    public List<DetailUserDTO> getAllByFirstName(String name) {
        return detailUserRepository.findAllByFirstNameContaining(name.toUpperCase())
                .stream()
                .map(DetailUserTransformer::getDetailUserDTOFromDetailUser)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsByIdDetailUser(Long id) {
        return detailUserRepository.existsByIdDetailUser(id);
    }
}

package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.domain.Department;
import com.tareas.asignacion_tareas.service.dto.DepartmentDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface DepartmentService {

    //get-Obtener-Red
    public List<DepartmentDTO> read();

    //Put-Actualizar-Update
    public DepartmentDTO update(DepartmentDTO departmentDTO);

    //Post-Create-Crear
    public ResponseEntity create(DepartmentDTO departmentDTO);

    //GetById - obtebner por Id
    public Optional<DepartmentDTO> getById(Long id);

    //GetByDepartment-Buscar por departamento
    public Optional<DepartmentDTO> getByDepartment(String department);

    //Traer todo de departamento
    public List<DepartmentDTO> getByDepartmentContaining(String department);
}

package com.tareas.asignacion_tareas.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class DetailUserDTO {

    @Id
    private Long idDetailUser;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 30, message = "El tamaño del campo debe de estar entre 2 a 30 caracteres")
    private String lastName;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 2, max = 30, message = "El tamaño del campo debe de estar entre 2 a 30 caracteres")
    private String firstName;

    @Column(nullable = false)
    @NotEmpty(message = "El campo es requerido")
    private String typeDocument;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 20, message = "El tamaño del campo debe de estar entre 6 a 20 caracteres")
    private String documentNumber;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 50, message = "El tamaño del campo debe de estar entre 6 a 50 caracteres")
    @Email(message = "El email no tiene la estructura correcta")
    private String email;

    @NotEmpty(message = "El campo es requerido")
    @Size(min = 6, max = 20, message = "El tamaño del campo debe de estar entre 6 a 20 caracteres")
    private String phoneNumber;

    @Size(min = 6, max = 50, message = "El tamaño del campo debe de estar entre 6 a 50 caracteres")
    private String address;

    private String fullName;

    private String documentfullName;
}

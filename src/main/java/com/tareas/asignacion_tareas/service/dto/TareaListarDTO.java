package com.tareas.asignacion_tareas.service.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class TareaListarDTO {

    private Long idTarea;
    private Long idDepartment;
    private String fullName;
    private String fullNameJefeDirecto;
    private Long idJefeDirecto;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime fechaTarea;
    private String descripcionTarea;
    private Boolean tareaActiva;


}

package com.tareas.asignacion_tareas.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tareas.asignacion_tareas.domain.Department;
import com.tareas.asignacion_tareas.domain.Users;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
public class TareaDTO {

    @Id
    private long idTarea;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime fechaTarea;

    @NotNull
    @Size(min = 2, max = 500, message = "La tarea debe contener entre 2 y 500 caracteres")
    private String descripcionTarea;

    @ManyToOne
    private Users empleado;

    @ManyToOne
    private Users jefeDirecto;

    @ManyToOne
    private Department departamento;

    private Boolean tareaActiva;


}

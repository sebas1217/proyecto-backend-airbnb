package com.tareas.asignacion_tareas.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

@Getter
@Setter
public class DepartmentDTO {

    @Id
    private Long idDepartamento;

    private String nombreDepartamento;

    private String descripcion;
}

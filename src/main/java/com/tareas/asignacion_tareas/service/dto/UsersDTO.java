package com.tareas.asignacion_tareas.service.dto;

import com.tareas.asignacion_tareas.domain.DetailUser;
import com.tareas.asignacion_tareas.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
public class UsersDTO {

    @Id
    private long idUser;

    @JoinColumn(name = "id_detail_user", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private DetailUser detailUser;

    @ManyToMany
    @JoinTable(name = "user_has_rol",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private Set<Rols> rols;
}

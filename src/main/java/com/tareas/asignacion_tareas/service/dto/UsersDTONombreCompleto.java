package com.tareas.asignacion_tareas.service.dto;

import com.tareas.asignacion_tareas.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
public class UsersDTONombreCompleto {

    @NotNull
    private Long idUsers;
    private String fullName;
    private String typeDocument;
    private String documentNumber;
    private String email;
    private String phoneNumber;
    private String address;
    private Set<Rols> rols;
}

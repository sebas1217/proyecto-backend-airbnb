package com.tareas.asignacion_tareas.service.dto;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class RolsDTO {

    @Id
    private Long idRols;

    @Size(min = 2, max = 50, message = "El tamaño del campo debe de estar entre 2 a 50 caracteres")
    @NotEmpty(message = "El campo es requerido")
    private String description;

}

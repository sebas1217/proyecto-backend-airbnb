package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.domain.Users;
import com.tareas.asignacion_tareas.repository.DetailUserRepository;
import com.tareas.asignacion_tareas.repository.UsersRepository;
import com.tareas.asignacion_tareas.service.dto.UsersDTO;
import com.tareas.asignacion_tareas.service.dto.UsersDTONombreCompleto;
import com.tareas.asignacion_tareas.service.transformer.UsersTransformer;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsersServiceImple implements UsersService{

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    DetailUserRepository detailUserRepository;

    @Override
    public Iterable<UsersDTONombreCompleto> read() {
        return usersRepository.findAll()
                .stream()
                .map(UsersTransformer::getUsersDTONombreCompletoFromUsers)
                .collect(Collectors.toList());
    }

    @Override
    public Users update(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public UsersDTO create(UsersDTO usersDTO) {
        Users users = UsersTransformer.getUsersFromUsersDTO(usersDTO);
        return UsersTransformer.getUsersDTOFromUsers(usersRepository.save(users));
    }

    @Override
    public Optional<UsersDTONombreCompleto> getById(Long id) throws IOException {
         Optional<Users>users = usersRepository.findById(id);

         return users.map(UsersTransformer::getUsersDTONombreCompletoFromUsers);
    }

    @Override
    public Optional<UsersDTO> getByDocumentNumber(String documentNumber) {
        return usersRepository.findByDetailUser_DocumentNumber(documentNumber)
                .map(UsersTransformer::getUsersDTOFromUsers);
    }

    @Override
    public Iterable<Users> getByDocumentNumberContaining(String documentNumber) {
        return usersRepository.findByDetailUser_DocumentNumberContaining(documentNumber);
    }

    @Override
    public Iterable<Users> getByFirstNameAndLastName(String firstName, String lastName) {
        return usersRepository.findByDetailUser_FirstNameAndDetailUser_LastName(firstName,lastName);
    }

    @Override
    public boolean existsByDocumentNumber(String documentNumber) {
        return usersRepository.existsByDetailUser_DocumentNumber(documentNumber);
    }

    @Override
    public boolean existsByFirstName(String firsName) {
        return usersRepository.existsByDetailUser_DocumentNumber(firsName);
    }

    @Override
    public List<UsersDTO> getAllByFirstName(String name) {
        return usersRepository.findAllByDetailUser_FirstNameContaining(name)
                .stream()
                .map(UsersTransformer::getUsersDTOFromUsers)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<UsersDTO> getByIdDetailUser(Long id) {
        return usersRepository.findByDetailUserIdDetailUser(id)
                .map(UsersTransformer::getUsersDTOFromUsers);
    }

    @Override
    public boolean existsByIdUser(Long id) {
        return usersRepository.existsByIdUser(id);
    }
}

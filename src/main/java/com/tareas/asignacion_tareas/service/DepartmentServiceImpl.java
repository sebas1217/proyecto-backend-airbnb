package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.domain.Department;
import com.tareas.asignacion_tareas.repository.DepartmenRepository;
import com.tareas.asignacion_tareas.service.dto.DepartmentDTO;
import com.tareas.asignacion_tareas.service.transformer.DepartmentTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    DepartmenRepository departmenRepository;

    @Override
    public List<DepartmentDTO> read() {
        return departmenRepository.findAll()
                .stream()
                .map(DepartmentTransformer::getDepartmentDTOFromDepartment)
                .collect(Collectors.toList());
    }

    @Override
    public DepartmentDTO update(DepartmentDTO departmentDTO) {
        Department department = DepartmentTransformer.getDepartmentFromDepartmentDTO(departmentDTO);
        return (DepartmentTransformer.getDepartmentDTOFromDepartment(departmenRepository.save(department))) ;
    }

    @Override
    public ResponseEntity create(DepartmentDTO departmentDTO) {
        Department department = DepartmentTransformer.getDepartmentFromDepartmentDTO(departmentDTO);
        return new ResponseEntity(DepartmentTransformer.getDepartmentDTOFromDepartment(departmenRepository.save(department)), HttpStatus.OK);
    }

    @Override
    public Optional<DepartmentDTO> getById(Long id) {
        return departmenRepository.findById(id)
                .map(DepartmentTransformer::getDepartmentDTOFromDepartment);
    }

    @Override
    public Optional<DepartmentDTO> getByDepartment(String department) {
        return departmenRepository.findByNombreDepartamentoContaining(department)
                .map(DepartmentTransformer::getDepartmentDTOFromDepartment);
    }

    @Override
    public List<DepartmentDTO> getByDepartmentContaining(String department) {
        return null;
    }
}

package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.service.dto.RolsDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface RolsService {
    //get - obtener - read
    public List<RolsDTO> read();

    //put - actualizar - update
    public RolsDTO update(RolsDTO rolsDTO);

    //post - create - crear
    public ResponseEntity create(RolsDTO rolsDTO);

    //getById obtener, read for id
    public Optional<RolsDTO> getById(Long id);
}

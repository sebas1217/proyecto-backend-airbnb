package com.tareas.asignacion_tareas.service.transformer;

import com.tareas.asignacion_tareas.domain.Users;
import com.tareas.asignacion_tareas.service.dto.UsersDTO;
import com.tareas.asignacion_tareas.service.dto.UsersDTONombreCompleto;

public class UsersTransformer {

    public static UsersDTO getUsersDTOFromUsers(Users users){
        if (users == null){
            return null;
        }

        UsersDTO usersDTO = new UsersDTO();

        usersDTO.setIdUser(users.getIdUser());
        usersDTO.setDetailUser(users.getDetailUser());
        usersDTO.setRols(users.getRols());

        return usersDTO;
    }

    public static Users getUsersFromUsersDTO(UsersDTO usersDTO){

        if (usersDTO == null){
            return null;
        }

        Users users = new Users();

        users.setIdUser(usersDTO.getIdUser());
        users.setDetailUser(usersDTO.getDetailUser());
        users.setRols(usersDTO.getRols());

        return users;
    }

    public static UsersDTONombreCompleto getUsersDTONombreCompletoFromUsers(Users users){

        if (users == null){
            return null;
        }

        UsersDTONombreCompleto usersDTONombreCompleto = new UsersDTONombreCompleto();

        usersDTONombreCompleto.setIdUsers(users.getIdUser());
        usersDTONombreCompleto.setFullName(users.getDetailUser().getFirstName() + " " + users.getDetailUser().getLastName());
        usersDTONombreCompleto.setTypeDocument(users.getDetailUser().getTypeDocument());
        usersDTONombreCompleto.setDocumentNumber(users.getDetailUser().getDocumentNumber());
        usersDTONombreCompleto.setEmail(users.getDetailUser().getEmail());
        usersDTONombreCompleto.setPhoneNumber(users.getDetailUser().getPhoneNumber());
        usersDTONombreCompleto.setAddress(users.getDetailUser().getAddress());
        usersDTONombreCompleto.setRols(users.getRols());

        return usersDTONombreCompleto;
    }
}

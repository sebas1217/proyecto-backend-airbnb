package com.tareas.asignacion_tareas.service.transformer;

import com.tareas.asignacion_tareas.domain.Department;
import com.tareas.asignacion_tareas.service.dto.DepartmentDTO;

public class DepartmentTransformer {

    public static DepartmentDTO getDepartmentDTOFromDepartment(Department department){
        if (department == null){
            return null;
        }

        DepartmentDTO departmentDTO = new DepartmentDTO();

        departmentDTO.setIdDepartamento(department.getIdDepartamento());
        departmentDTO.setNombreDepartamento(department.getNombreDepartamento());
        departmentDTO.setDescripcion(department.getDescripcion());

        return departmentDTO;

    }

    public static Department getDepartmentFromDepartmentDTO(DepartmentDTO departmentDTO){
        if (departmentDTO == null){
            return null;
        }

        Department department = new Department();

        department.setIdDepartamento(departmentDTO.getIdDepartamento());
        department.setNombreDepartamento(departmentDTO.getNombreDepartamento());
        department.setDescripcion(departmentDTO.getDescripcion());

        return department;
    }
}

package com.tareas.asignacion_tareas.service.transformer;

import com.tareas.asignacion_tareas.domain.Tarea;
import com.tareas.asignacion_tareas.service.dto.TareaDTO;
import com.tareas.asignacion_tareas.service.dto.TareaListarDTO;

public class TareaTransformer {

    public static TareaDTO getTareaDTOFromTarea(Tarea tarea){
        if (tarea == null){
            return null;
        }

        TareaDTO tareaDTO = new TareaDTO();

        tareaDTO.setIdTarea(tarea.getIdTarea());
        tareaDTO.setFechaTarea(tarea.getFechaTarea());
        tareaDTO.setDescripcionTarea(tarea.getDescripcionTarea());
        tareaDTO.setDepartamento(tarea.getDepartamento());
        tareaDTO.setJefeDirecto(tarea.getJefeDirecto());
        tareaDTO.setEmpleado(tarea.getEmpleado());
        tareaDTO.setTareaActiva(tarea.getTareaActiva());

        return tareaDTO;
    }

    public static Tarea getTareaFromTareaDTO(TareaDTO tareaDTO){

        if(tareaDTO == null){
            return null;
        }

        Tarea tarea = new Tarea();

        tarea.setIdTarea(tareaDTO.getIdTarea());
        tarea.setDescripcionTarea(tareaDTO.getDescripcionTarea());
        tarea.setFechaTarea(tareaDTO.getFechaTarea());
        tarea.setDepartamento(tareaDTO.getDepartamento());
        tarea.setJefeDirecto(tareaDTO.getJefeDirecto());
        tarea.setEmpleado(tareaDTO.getEmpleado());
        tarea.setTareaActiva(tareaDTO.getTareaActiva());

        return tarea;
    }

    public static TareaListarDTO getTareaListarDTO(Tarea tarea){
        if (tarea == null){
            return null;
        }

        TareaListarDTO tareaListarDTO = new TareaListarDTO();

        tareaListarDTO.setIdTarea(tarea.getIdTarea());
        tareaListarDTO.setIdDepartment(tarea.getDepartamento().getIdDepartamento());
        tareaListarDTO.setIdJefeDirecto(tarea.getJefeDirecto().getIdUser());
        tareaListarDTO.setFullName(tarea.getEmpleado().getDetailUser().getFirstName() + " " + tarea.getEmpleado().getDetailUser().getLastName());
        tareaListarDTO.setFullNameJefeDirecto(tarea.getJefeDirecto().getDetailUser().getFirstName() +" " + tarea.getJefeDirecto().getDetailUser().getLastName());
        tareaListarDTO.setFechaTarea(tarea.getFechaTarea());
        tareaListarDTO.setDescripcionTarea(tarea.getDescripcionTarea());
        tareaListarDTO.setTareaActiva(tarea.getTareaActiva());

        return tareaListarDTO;
    }

}

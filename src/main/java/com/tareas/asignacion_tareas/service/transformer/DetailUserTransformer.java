package com.tareas.asignacion_tareas.service.transformer;

import com.tareas.asignacion_tareas.domain.DetailUser;
import com.tareas.asignacion_tareas.service.dto.DetailUserDTO;

public class DetailUserTransformer {

    public static DetailUserDTO getDetailUserDTOFromDetailUser(DetailUser detailUser){

        if (detailUser == null){
            return null;
        }

        DetailUserDTO detailUserDTO = new DetailUserDTO();

        detailUserDTO.setIdDetailUser(detailUser.getIdDetailUser());
        detailUserDTO.setLastName(detailUser.getLastName());
        detailUserDTO.setFirstName(detailUser.getFirstName());
        detailUserDTO.setTypeDocument(detailUser.getTypeDocument());
        detailUserDTO.setDocumentNumber(detailUser.getDocumentNumber());
        detailUserDTO.setEmail(detailUser.getEmail());
        detailUserDTO.setPhoneNumber(detailUser.getPhoneNumber());
        detailUserDTO.setAddress(detailUser.getAddress());
        detailUserDTO.setFullName(detailUser.getFirstName() + " " + detailUser.getLastName());
        detailUserDTO.setDocumentfullName(detailUser.getDocumentNumber() + " " + detailUser.getFirstName() + " " + detailUser.getLastName());

        return detailUserDTO;
    }
}

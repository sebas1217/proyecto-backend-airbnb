package com.tareas.asignacion_tareas.service.transformer;

import com.tareas.asignacion_tareas.domain.Rols;
import com.tareas.asignacion_tareas.service.dto.RolsDTO;

public class RolsTransformer {

    public static RolsDTO getRolsDTOFromRols(Rols rols){

        if (rols == null){
            return null;
        }

        RolsDTO rolsDTO = new RolsDTO();

        rolsDTO.setIdRols(rols.getIdRols());
        rolsDTO.setDescription(rols.getDescription());


        return rolsDTO;
    }

    public static Rols getRolsFromRolsDTO(RolsDTO rolsDTO){
        if (rolsDTO == null){
            return null;
        }

        Rols rols = new Rols();

        rols.setIdRols(rolsDTO.getIdRols());
        rols.setDescription(rolsDTO.getDescription());

        return rols;
    }
}

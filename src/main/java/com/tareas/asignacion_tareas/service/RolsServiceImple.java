package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.domain.Rols;
import com.tareas.asignacion_tareas.repository.RolsRepository;
import com.tareas.asignacion_tareas.service.dto.RolsDTO;
import com.tareas.asignacion_tareas.service.transformer.RolsTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RolsServiceImple implements RolsService{

    @Autowired
    RolsRepository rolsRepository;

    @Override
    public List<RolsDTO> read() {
        return rolsRepository.findAll()
                .stream()
                .map(RolsTransformer::getRolsDTOFromRols)
                .collect(Collectors.toList());
    }

    @Override
    public RolsDTO update(RolsDTO rolsDTO) {
        Rols rols = RolsTransformer.getRolsFromRolsDTO(rolsDTO);
        return (RolsTransformer.getRolsDTOFromRols(rolsRepository.save(rols)));
    }

    @Override
    public ResponseEntity create(RolsDTO rolsDTO) {
        Rols rols = RolsTransformer.getRolsFromRolsDTO(rolsDTO);
        return new ResponseEntity(RolsTransformer.getRolsDTOFromRols(rolsRepository.save(rols)),HttpStatus.OK);
    }

    @Override
    public Optional<RolsDTO> getById(Long id) {
        return rolsRepository.findById(id)
                .map(RolsTransformer::getRolsDTOFromRols);
    }
}

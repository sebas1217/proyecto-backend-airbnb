package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.domain.Tarea;
import com.tareas.asignacion_tareas.domain.Users;
import com.tareas.asignacion_tareas.repository.DepartmenRepository;
import com.tareas.asignacion_tareas.repository.TareaRepository;
import com.tareas.asignacion_tareas.repository.UsersRepository;
import com.tareas.asignacion_tareas.service.dto.TareaDTO;
import com.tareas.asignacion_tareas.service.dto.TareaListarDTO;
import com.tareas.asignacion_tareas.service.transformer.TareaTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TareaServiceImple  implements TareaService{

    @Autowired
    TareaRepository tareaRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    DepartmenRepository departmenRepository;

    @Override
    public Page<TareaDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return tareaRepository.findAll(pageable)
                .map(TareaTransformer::getTareaDTOFromTarea);
    }

    @Override
    public TareaDTO update(TareaDTO tareaDTO) {
        Tarea tarea = TareaTransformer.getTareaFromTareaDTO(tareaDTO);
        TareaDTO tareaDTO1 = TareaTransformer.getTareaDTOFromTarea(tareaRepository.save(tarea));
        return (TareaTransformer.getTareaDTOFromTarea(tareaRepository.save(tarea)));
    }

    @Override
    public ResponseEntity create(TareaDTO tareaDTO) {
        tareaDTO.setFechaTarea(LocalDateTime.now());
        Tarea tarea = TareaTransformer.getTareaFromTareaDTO(tareaDTO);

        Tarea tarea1 = tareaRepository.save(tarea);
        return new ResponseEntity(TareaTransformer.getTareaDTOFromTarea(tarea1), HttpStatus.OK);
    }

    @Override
    public Optional<TareaDTO> getById(@PathVariable Long id) {
        return tareaRepository.findById(id)
                .map(TareaTransformer::getTareaDTOFromTarea);
    }

    @Override
    public Long cantidadTareas() {
        return tareaRepository.cantidadTareas();
    }

    @Override
    public List<TareaListarDTO> getByTareaActiva(boolean tareaActiva) {
        return tareaRepository.findByTareaActiva(tareaActiva)
                .stream()
                .map(TareaTransformer::getTareaListarDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Page<TareaListarDTO> search(String documentNumber, Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        Users empleado = new Users();

        if (documentNumber != null){
            empleado = usersRepository.findByDetailUser_DocumentNumber(documentNumber).get();
        }
        return tareaRepository.findByEmpleado(empleado, pageable)
                .map(TareaTransformer::getTareaListarDTO);
    }

    @Override
    public Page<TareaListarDTO> getByNameEmpleado(String nombreEmpleado, Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);

        Users empleado = new Users();

        if (nombreEmpleado != null){
            empleado = usersRepository.findByDetailUser_FirstNameContaining(nombreEmpleado.toUpperCase()).get();
        }
        return tareaRepository.findByEmpleado(empleado,pageable)
                .map(TareaTransformer::getTareaListarDTO);
    }

    
}

package com.tareas.asignacion_tareas.service;

import com.tareas.asignacion_tareas.service.dto.TareaDTO;
import com.tareas.asignacion_tareas.service.dto.TareaListarDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface TareaService {

    //Get
    public Page<TareaDTO> read(Integer pageSize, Integer pageNumber);

    //Put
    public TareaDTO update(TareaDTO tareaDTO);

    //post
    public ResponseEntity create (TareaDTO tareaDTO);

    //Tomar por id
    public Optional<TareaDTO> getById(Long id);

    //Cantidad de taras
    public Long cantidadTareas();

    //listar por estado
    public List<TareaListarDTO> getByTareaActiva(boolean tareaActiva);

    //Listar por documento empleado
    public Page<TareaListarDTO> search(String documentNumber, Integer pageSize, Integer pageNumber);

    //Listar por nombre empelado
    public Page<TareaListarDTO> getByNameEmpleado(String nombreEmpleado, Integer pageSize, Integer pageNumber);
}

package com.tareas.asignacion_tareas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsignacionTareasApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsignacionTareasApplication.class, args);
    }

}

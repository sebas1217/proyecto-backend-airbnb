package com.tareas.asignacion_tareas.repository;

import com.tareas.asignacion_tareas.domain.Rols;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolsRepository extends JpaRepository <Rols, Long> {
}

package com.tareas.asignacion_tareas.repository;

import com.tareas.asignacion_tareas.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByDetailUser_DocumentNumber(String documentNumber);

    Iterable<Users> findByDetailUser_DocumentNumberContaining(String documentNumber); //findByDocumentNumberContaining

    Iterable<Users> findByDetailUser_FirstNameAndDetailUser_LastName(String firstName, String lastName); //findByFirstNameAndLastName


    Optional<Users> findByDetailUser_FirstNameContaining(String nameApprentice); //findByFirstNameContaining

    Boolean existsByDetailUser_DocumentNumber(String documentNumber); //existsByDocumentNumber

    Boolean existsByDetailUser_FirstName(String firstName); //existsByFirstName

    /*@Query(value = "select u from Users u where UPPER(u.firstName) like %UPPER(:nameApprentice)%")
    Optional<Users> findByFirstNameContainingQuery(String nameApprentice);*/

    List<Users> findAllByDetailUser_FirstNameContaining(String name); //findAllByFirstNameContaining

    Optional<Users> findByDetailUserIdDetailUser(Long id);

    Users findByDetailUser_Email (String email);

    Boolean existsByIdUser(Long id);
}

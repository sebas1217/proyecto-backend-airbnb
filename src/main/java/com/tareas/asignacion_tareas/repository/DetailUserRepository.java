package com.tareas.asignacion_tareas.repository;

import com.tareas.asignacion_tareas.domain.DetailUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DetailUserRepository extends JpaRepository<DetailUser, Long> {

    List<DetailUser> findByDocumentNumberContaining(String documentNumber); //findByDocumentNumberContaining

    List<DetailUser> findAllByFirstNameAndLastNameContaining(String firstName, String lastName); //findByDetailUser_FirstNameAndDetailUser_LastName

    List<DetailUser> findAllByFirstNameContaining (String name); //findAllByFirstNameContaining

    Boolean existsByIdDetailUser(Long id);
}

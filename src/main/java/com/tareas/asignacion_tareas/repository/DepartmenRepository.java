package com.tareas.asignacion_tareas.repository;

import com.tareas.asignacion_tareas.domain.Department;
import com.tareas.asignacion_tareas.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DepartmenRepository extends JpaRepository<Department, Long> {

    Optional<Department> findByNombreDepartamentoContaining(String nombreDepartamento);

    List<Department> findAllByNombreDepartamentoContains(String nombreDepartamento);

}

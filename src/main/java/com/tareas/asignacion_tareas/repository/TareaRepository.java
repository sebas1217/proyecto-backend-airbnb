package com.tareas.asignacion_tareas.repository;

import com.tareas.asignacion_tareas.domain.Department;
import com.tareas.asignacion_tareas.domain.Tarea;
import com.tareas.asignacion_tareas.domain.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TareaRepository extends JpaRepository<Tarea, Long> {

    List<Tarea>findByDepartamento(Department departamento);

    List<Tarea>findByTareaActiva(Boolean tareaActiva);

    List<Tarea>findByTareaActivaIsTrueOrderByFechaTarea();


    Page<Tarea>findByEmpleado(Users empelado, Pageable pageable);

    @Query(value ="select count (tarea) from Tarea tarea")
    Long cantidadTareas();
}
